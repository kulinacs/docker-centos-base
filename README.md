Docker CentOS Base
==================

[Docker Hub](https://hub.docker.com/r/kulinacs/centos-base/)
[GitLab](https://gitlab.com/kulinacs/docker-centos-base)

The Docker CentOS base container serves as a base container.

Docker Compose
--------------

`docker-compose` can be used for local testing. Running `docker-compose up --build` will rebuild the centos-base container.

GitLab CI
---------

GitLabCI is used to build and deploy the container to DockerHub.

1) The container is built and pushed to the internal GitLab container registry.

2) The container is run as a service and tested to verify Ansible can run on the container remotely.

3) If tagged, the container is pushed to Dockerhub.
